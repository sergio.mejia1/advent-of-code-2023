import unittest
from day_08 import day_08


class Day08(unittest.TestCase):
  def test_task_1(self):
    lines = [
      "RL",
      "",
      "AAA = (BBB, CCC)",
      "BBB = (DDD, EEE)",
      "CCC = (ZZZ, GGG)",
      "DDD = (DDD, DDD)",
      "EEE = (EEE, EEE)",
      "GGG = (GGG, GGG)",
      "ZZZ = (ZZZ, ZZZ)",
    ]
    self.assertEqual(day_08.task_1(lines), 2)

  def test_other_task_1(self):
    lines = [
      "LLR",
      "",
      "AAA = (BBB, BBB)",
      "BBB = (AAA, ZZZ)",
      "ZZZ = (ZZZ, ZZZ)",
    ]
    self.assertEqual(day_08.task_1(lines), 6)

  """
  def test_task_2(self):
    lines = []
    self.assertEqual(day_08.task_2(lines), 0)
  """

if __name__ == '__main__':
    unittest.main()