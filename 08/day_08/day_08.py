import re
from collections import defaultdict

def task_1(lines):
  dirs = lines[0]
  directions = defaultdict(tuple)
  for line in lines[2:]:
    match = re.findall(r"(\w+) = \((\w+), (\w+)\)", line)
    (_from, left, right) = match[0]
    directions[_from] = (left, right)
  
  start = directions["AAA"]
  found = False
  i = 0
  count = 0
  while not found:
    dir = dirs[i]
    idx = 0 if dir == "L" else 1
    dest = start[idx]
    start = directions[dest]
    count += 1
    if dest == "ZZZ":
      found = True
    i = (i + 1) % len(dirs)
  return count

if __name__ == "__main__":
  f = open("in_08.txt", "r")
  lines = f.readlines()
  lines = [line.strip() for line in lines]

  print(f"Task 1 solution: {task_1(lines)}")

  f.close()