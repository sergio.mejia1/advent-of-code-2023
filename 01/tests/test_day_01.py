import unittest
from day_01 import day_01


class Day01(unittest.TestCase):
  def test_task_1(self):
    lines = ["1abc2",
    "pqr3stu8vwx",
    "a1b2c3d4e5f",
    "treb7uchet"]
    self.assertEqual(day_01.task_1(lines), 142)

  def test_task_2(self):
    lines = ["two1nine",
      "eightwothree",
      "abcone2threexyz",
      "xtwone3four",
      "4nineeightseven2",
      "zoneight234",
      "7pqrstsixteen"]
    self.assertEqual(day_01.task_2(lines), 281)

if __name__ == '__main__':
    unittest.main()