digits = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
digits_extended = {**{d: int(d) for d in digits}, 
                   **{"one": 1, "two": 2, "three": 3, "four": 4, "five": 5, "six": 6, "seven": 7, "eight": 8, "nine": 9}}

def task_1(lines):
  sum = 0
  for line in lines:
    i = 0
    while line[i] not in digits:
      i += 1
    j = len(line)-1
    while line[j] not in digits:
      j -= 1

    n = int(line[i] + line[j])
    sum += n
  return sum

def starts_with(literal: str):
  for digit in digits_extended:
    if literal.startswith(digit):
      return digits_extended[digit]
  return None

def task_2(lines):
  another_sum = 0
  for line in lines:
    i = 0
    val_1 = starts_with(line[i:])
    while val_1 is None:
      i += 1
      val_1 = starts_with(line[i:])
    j = len(line)-1
    val_2 = starts_with(line[j:]) 
    while val_2 is None:
      j -= 1
      val_2 = starts_with(line[j:])
    n = val_1*10 + val_2
    another_sum += n
  return another_sum

if __name__ == '__main__':
  f = open("in_01.txt", "r")
  lines = f.readlines()
  f.close()
  lines = [line.strip() for line in lines]
  print("Solution to task 1", task_1(lines))

  print("Solution to task 2", task_2(lines))