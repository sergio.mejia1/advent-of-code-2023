import unittest
from day_11 import day_11


class Day11(unittest.TestCase):

  def test_expand_space(self):
    original = [
      "...#......",
      ".......#..",
      "#.........",
      "..........",
      "......#...",
      ".#........",
      ".........#",
      "..........",
      ".......#..",
      "#...#.....",
    ]
    original = [list(line.strip()) for line in original]


    expected = [
      "....#........",
      ".........#...",
      "#............",
      ".............",
      ".............",
      "........#....",
      ".#...........",
      "............#",
      ".............",
      ".............",
      ".........#...",
      "#....#.......",
    ]
    expected = [list(line.strip()) for line in expected]


    self.assertEqual(day_11.expand_space(original), expected)

  def test_task_1(self):
    expected = [
      "....#........",
      ".........#...",
      "#............",
      ".............",
      ".............",
      "........#....",
      ".#...........",
      "............#",
      ".............",
      ".............",
      ".........#...",
      "#....#.......",
    ]
    expected = [list(line.strip()) for line in expected]
    (_, sol) = day_11.task_1(expected)
    self.assertEqual(sol, 374)

  
  
  def test_task_2(self):
    original = [
      "...#......",
      ".......#..",
      "#.........",
      "..........",
      "......#...",
      ".#........",
      ".........#",
      "..........",
      ".......#..",
      "#...#.....",
    ]
    
    _, sol = day_11.task_2(original, 10)
    self.assertEqual(sol, 1030)
  

if __name__ == '__main__':
    unittest.main()