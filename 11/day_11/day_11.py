from copy import deepcopy
import time 

SPACE = "."
GALAXY = "#"

def print_space(space):
  for row in space:
    print("".join(row))

def expand_space(space: list):
  new_space = deepcopy(space)
  delta_i, delta_j = 0, 0
  for j in range(len(space)):
    if not any([cell == GALAXY for cell in space[j]]):
      new_space.insert(j+delta_j, space[j])
      delta_j += 1
  for i in range(len(space[0])):
    if not any([space[j][i] == GALAXY for j in range(len(space))]):
      for j in range(len(new_space)): # new_space because we've already added new rows!
        new_space[j].insert(i+delta_i, ".")
      delta_i += 1
  # print_space(new_space)
  return new_space

def find_galaxies(space) -> list:
  galaxies = []
  for j in range(len(space)):
    for i in range(len(space[j])):
      if space[j][i] == GALAXY:
        galaxies.append((j,i))
  return galaxies

def manhattan(p1, p2):
  return abs(p1[1]-p2[1]) + abs(p1[0]-p2[0])

def task_1(space):
  galaxies = find_galaxies(space)
  sum = 0
  d_matrix = [[0]*len(galaxies)]*len(galaxies)
  for i in range(len(galaxies)):
    for j in range(i+1, len(galaxies)):
      d = manhattan(galaxies[i], galaxies[j])
      d_matrix[i][j] = d_matrix[j][i] = d
      sum += d
  return (d_matrix, sum)

def task_2(space, EXPANSION_FACTOR = 1_000_000):
  expanded_i, expanded_j = [], []
  for j in range(len(space)):
    if not any([cell == GALAXY for cell in space[j]]):
      expanded_j.append(j)
  for i in range(len(space[0])):
    if not any([space[j][i] == GALAXY for j in range(len(space))]):
      expanded_i.append(i)
  galaxies = find_galaxies(space)
  sum = 0
  d_matrix = [[0]*len(galaxies)]*len(galaxies)
  for i in range(len(galaxies)):
    for j in range(i+1, len(galaxies)):
      d = manhattan(galaxies[i], galaxies[j])
      n = 0
      for exp in expanded_i:
        if min(galaxies[i][1], galaxies[j][1]) < exp < max(galaxies[i][1], galaxies[j][1]):
          n += 1
      for exp in expanded_j:
        if min(galaxies[i][0], galaxies[j][0]) < exp < max(galaxies[i][0], galaxies[j][0]):
          n += 1
      d_matrix[i][j] = d_matrix[j][i] = d + n*(EXPANSION_FACTOR-1) # -1 since we already have 1 row/col (the original one)
      sum += d_matrix[i][j]
  return (d_matrix, sum)

if __name__ == "__main__":
  f = open("in_11.txt", "r")
  lines = f.readlines()
  lines = [list(line.strip()) for line in lines]
  f.close()

  # Version 1. Time efficient, space inefficient
  start = time.time()
  expanded = expand_space(lines)
  (min_d_matrix, sum) = task_1(expanded)
  print(f"Task 1 solution: {sum}")
  print(f"Elapsed time: {(time.time()-start):.2f}s")
  
  # Version 2. Reusing Task 2 logic. Time inefficient, space efficient
  start = time.time()
  (min_d_matrix, sum) = task_2(lines, EXPANSION_FACTOR=2)
  print(f"Task 1 solution: {sum}")
  print(f"Elapsed time: {(time.time()-start):.2f}s")

  start = time.time()
  (min_d_matrix, sum) = task_2(lines)
  print(f"Task 2 solution: {sum}")
  print(f"Elapsed time: {(time.time()-start):.2f}s")
  
