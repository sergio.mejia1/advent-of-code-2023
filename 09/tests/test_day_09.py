import unittest
from day_09 import day_09

lines = [
  "0 3 6 9 12 15",
  "1 3 6 10 15 21",
  "10 13 16 21 30 45",
]

class Day09(unittest.TestCase):

  def test_task_1(self):
    self.assertEqual(day_09.task_1(lines), 114)

  
  def test_task_2(self):
    self.assertEqual(day_09.task_2(lines), 2)
  

if __name__ == '__main__':
    unittest.main()