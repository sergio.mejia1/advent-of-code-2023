
def create_reductions(reading):
  reductions = [reading]
  while not all([i==0 for i in reductions[-1]]):
    new_reading = [reductions[-1][i] - reductions[-1][i-1] for i in range(1,len(reductions[-1]))]
    reductions.append(new_reading)
  return reductions

def task_1(lines):
  num_lines = [[int(n) for n in line.split(" ")] for line in lines]
  total = 0
  for reading in num_lines:
    reductions = create_reductions(reading)    
    prediction = sum([reductions[i][-1] for i in range(len(reductions))])
    total += prediction
  return total

def task_2(lines):
  num_lines = [[int(n) for n in line.split(" ")] for line in lines]
  total = 0
  for reading in num_lines:
    reductions = create_reductions(reading)    
    diff = 0
    for i in range(len(reductions)-1, -1, -1):
      diff = reductions[i][0] - diff
    # Also works byt is not as clean 
    # prediction = reductions[0][0] - sum([(-1 if i%2==0 else 1)*reductions[i][0] for i in range(1,len(reductions))])
    total += diff
  return total

if __name__ == "__main__":
  f = open("in_09.txt", "r")
  lines = f.readlines()
  lines = [line.strip() for line in lines]

  f.close()

  print(f"Task 1: {task_1(lines)}")
  print(f"Task 2: {task_2(lines)}")