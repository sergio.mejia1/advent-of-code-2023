#!/bin/bash

day=$(date +%d)
if [ $# -ne 0 ] 
  then
    day=$1
fi

poetry new $day --name day_$day
cd $day
cp ../templates/python/base.py day_$day/day_$day.py
sed -i "s/{{day}}/$day/" day_$day/*
cp ../templates/python/testbase.py tests/test_day_$day.py
sed -i "s/{{day}}/$day/" tests/*


cd ..
AOC_SESSION_COOKIE=$(<.credentials) 
echo https://adventofcode.com/2023/day/$((10#$day))/input
curl --cookie "session=${AOC_SESSION_COOKIE}" https://adventofcode.com/2023/day/$((10#$day))/input -o $day/in_$day.txt --create-dirs
