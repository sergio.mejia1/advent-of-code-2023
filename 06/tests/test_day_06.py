import unittest
from day_06 import day_06


class Day06(unittest.TestCase):
  def test_task_1(self):
    lines = [
      "Time:      7  15   30",
      "Distance:  9  40  200"
    ]
    self.assertEqual(day_06.task_1(lines), 288)

  
  def test_task_2(self):
    lines = [
      "Time:      7  15   30",
      "Distance:  9  40  200"
    ]
    self.assertEqual(day_06.task_2(lines), 71503)

  def test_improvements(self):
    lines = [
      "Time:      7  15   30",
      "Distance:  9  40  200"
    ]
    self.assertEqual(day_06.task_1(lines), day_06.task_1_improved(lines))
    self.assertEqual(day_06.task_2(lines), day_06.task_2_improved(lines))


if __name__ == '__main__':
    unittest.main()