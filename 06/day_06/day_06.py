import re
import time

def task_1(lines):
#  lines[0] = lines[0].replace(" ", "")
#  lines[1] = lines[1].replace(" ", "")

  times = [int(num) for num in re.findall(r'\d+', lines[0])]
  distance = [int(num) for num in re.findall(r'\d+', lines[1])]

  print(times, distance)

  n_races = len(times)
  res = 1
  for i in range(n_races):
    sum = 0
    for j in range(times[i]):
      if j * (times[i]-j) > distance[i]:
        sum += 1
    res *= sum
  return res

def task_2(lines):
  lines[0] = lines[0].replace(" ", "")
  lines[1] = lines[1].replace(" ", "")
  return task_1(lines)

def task_1_improved(lines):
  # D = x * t
  # D = x * (T - x)
  # D = Tx - x^2
  # (-1)x^2 + (T)x + (-D) = 0
  # Quadratic formula: a=-1, b=T, c=-D
  import math
  times = [int(num) for num in re.findall(r'\d+', lines[0])]
  distance = [int(num) for num in re.findall(r'\d+', lines[1])]

  a = -1
  n_races = len(times)
  res = 1
  for i in range(n_races):
    b = times[i]
    c = -distance[i]
    boundaryOne = (-b + math.sqrt(b**2 - 4*a*c))/(2*a) 
    boundaryTwo = (-b - math.sqrt(b**2 - 4*a*c))/(2*a) 
    # Rounding shenanigans
    values = abs(int(boundaryOne) - int(boundaryTwo))
    if boundaryOne.is_integer() and boundaryTwo.is_integer():
      values -= 1
    res *= int(values)
  return res

def task_2_improved(lines):
  lines[0] = lines[0].replace(" ", "")
  lines[1] = lines[1].replace(" ", "")
  return task_1_improved(lines)


if __name__ == "__main__":
  f = open("in_06.txt", "r")
  lines = f.readlines()
  lines = [line.strip() for line in lines]

  f.close()
  print("Task 1:", task_1(lines))
  print("Task 1 improved:", task_1_improved(lines))
  start = time.time()
  print("Task 2:", task_2(lines))
  print(f"Elapsed time for task 2: {time.time()-start:.2f}s")
  start = time.time()
  print("Task 2 improved:", task_2_improved(lines))
  print(f"Elapsed time for task 2 improved: {((time.time()-start)*1000):.2f}ms")