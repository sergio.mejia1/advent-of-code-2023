import unittest
from day_10 import day_10


class Day10(unittest.TestCase):
  def test_task_1(self):
    lines = [
      "..F7.",
      ".FJ|.",
      "SJ.L7",
      "|F--J",
      "LJ...",
    ]
    self.assertEqual(day_10.task_1(lines), 8)

  
  def test_task_2(self):
    lines = [
      "FF7FSF7F7F7F7F7F---7",
      "L|LJ||||||||||||F--J",
      "FL-7LJLJ||||||LJL-77",
      "F--JF--7||LJLJIF7FJ-",
      "L---JF-JLJIIIIFJLJJ7",
      "|F|F-JF---7IIIL7L|7|",
      "|FFJF7L7F-JF7IIL---7",
      "7-L-JL7||F7|L7F-7F7|",
      "L.L7LFJ|||||FJL7||LJ",
      "L7JLJL-JLJLJL--JLJ.L",
    ]
    self.assertEqual(day_10.task_2(lines), 10)
  

if __name__ == '__main__':
    unittest.main()