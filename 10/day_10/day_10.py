
from enum import Enum

VERTICAL = "|"
HORIZONTAL = "-"
NORTH_EAST = "L"
NORTH_WEST = "J" #-'
SOUTH_WEST = "7" # -,
SOUTH_EAST = "F" # ,-
GROUND = "."
START = "S"

NORTH = "north"
SOUTH = "south"
WEST = "west"
EAST = "east"

"""
"Going to X is only possible if coming from Y positioned at dir
{ X: {Y: dir}}
"""
matches = {
  VERTICAL: { START: [NORTH, SOUTH], VERTICAL: [NORTH, SOUTH], SOUTH_WEST: [NORTH], SOUTH_EAST: [NORTH], NORTH_EAST: [SOUTH], NORTH_WEST: [SOUTH]},
  HORIZONTAL: { START: [EAST, WEST], HORIZONTAL: [EAST, WEST], NORTH_EAST: [WEST], NORTH_WEST: [EAST], SOUTH_WEST: [EAST], SOUTH_EAST: [WEST]},
  NORTH_EAST: { START: [NORTH, EAST], VERTICAL: [NORTH], HORIZONTAL: [EAST], NORTH_WEST: [EAST], SOUTH_WEST: [EAST, NORTH], SOUTH_EAST: [NORTH]},
  NORTH_WEST: { START: [NORTH, WEST], VERTICAL: [NORTH], HORIZONTAL: [WEST], NORTH_EAST: [WEST], SOUTH_WEST: [NORTH], SOUTH_EAST: [NORTH, WEST]},
  SOUTH_WEST: { START: [SOUTH, WEST], VERTICAL: [SOUTH], HORIZONTAL: [WEST], NORTH_EAST: [WEST, SOUTH], NORTH_WEST: [SOUTH], SOUTH_EAST: [WEST]},
  SOUTH_EAST: { START: [SOUTH, EAST], VERTICAL: [SOUTH], HORIZONTAL: [EAST], NORTH_EAST: [SOUTH], NORTH_WEST: [SOUTH, EAST], SOUTH_WEST: [EAST]},
  GROUND: {}
}

"""
When coming to X from dir Y, move delta and now you'll be moving D
{X: {Y: (delta, D)}}
"""
next = {
  VERTICAL: { NORTH: (1,0, NORTH), SOUTH: (-1,0, SOUTH) },
  HORIZONTAL: { EAST: (0,-1, EAST), WEST: (0,1, WEST) },
  NORTH_EAST: { NORTH: (0, 1, WEST), EAST: (-1,0, SOUTH)},
  NORTH_WEST: { NORTH: (0, -1, EAST), WEST: (-1,0, SOUTH)},
  SOUTH_WEST: { SOUTH: (0, -1, EAST), WEST: (1,0, NORTH)},
  SOUTH_EAST: { SOUTH: (0, 1, WEST), EAST: (1,0, NORTH)},
  START: { NORTH: (1,0, NORTH), SOUTH: (-1,0, SOUTH), EAST: (0,-1, EAST), WEST: (0,1, WEST)},
  GROUND: {}
}

def advance(map, originCoord, originDir):
  originPipe = map[originCoord[0]][originCoord[1]]
  if originPipe in next:
    nextDir = next[originPipe][originDir]
    nextCoord = (originCoord[0] + nextDir[0], originCoord[1] + nextDir[1])
    maybeNextPipe = map[nextCoord[0]][nextCoord[1]]
    if maybeNextPipe in matches:
      if originPipe in matches[maybeNextPipe]:
        if nextDir[2] in matches[maybeNextPipe][originPipe]: 
          return (*nextCoord, nextDir[2])
    else:
      return nextCoord
  return None

def find_loop(lines):
  start_pos = (-1,-1)
  for j in range(len(lines)):
    for i in range(len(lines[j])):
      if lines[j][i] == START:
        start_pos = (j,i)
        break
  
  print(start_pos)
  found_loop = False
  loop = []

  dirs = [NORTH, SOUTH, EAST, WEST]
  i = 0
  while i < len(dirs) and not found_loop:
    loop = [start_pos]
    dir = dirs[i]
    i += 1
    next_coord = advance(lines, start_pos, dir)
    while next_coord is not None and 0 <= next_coord[0] < len(lines) and 0 <= next_coord[1] < len(lines[0]):
      loop.append(next_coord)
      next_coord = advance(lines, (next_coord[0],next_coord[1]), next_coord[2])
      if next_coord == start_pos:
        found_loop = True
        break
  if not found_loop:
    return None
  return loop

def task_1(lines):
  loop = find_loop(lines)
  print(loop)
  if loop is None:
    raise Exception("Not loop found! D:")
  return len(loop)//2

"""
def replace_start(map, loop):
  diff_j = loop[-1][0] - loop[1][0]
  diff_i = loop[-1][1] - loop[1][1]

  if abs(diff_j) == 2:
    map[loop[0][0]][loop[0][1]] = VERTICAL
  if abs(diff_i) == 2:
    map[loop[0][0]][loop[0][1]] = HORIZONTAL
  if diff_i == 1 and diff_j == 1:# i=1 is right, j =1 is down 
    map[loop[0][0]][loop[0][1]] = NORTH_EAST
  if diff_i == 1 and diff_j == -1:# i=1 is right, j =-1 is up
    map[loop[0][0]][loop[0][1]] = SOUTH_EAST
  if diff_i == -1 and diff_j == 1:# i=-1 is left, j =1 is down 
    map[loop[0][0]][loop[0][1]] = NORTH_WEST
  if diff_i == -1 and diff_j == -1:# i=1 is right, j = 
    map[loop[0][0]][loop[0][1]] = SOUTH_WEST
"""


def find_in_loop(loop, j, i):
  for idx, input in enumerate(loop):
    if input[0] == j and input[1] == i:
      return idx
  return None

def task_2(lines):
  loop = find_loop(lines)
  if loop is None:
    raise Exception("Not loop found! D:")
  lines = [list(line) for line in lines]
  #replace_start(lines, loop)
  count = 0
  isInLoop = False
  for j in range(len(lines)):
    for i in range(len(lines[j])):
        # FIXME This is O(n3) as it stands, which finishes but it's not nice
        cell_in_loop = find_in_loop(loop, j, i)
        down_cell_in_loop = find_in_loop(loop, j+1, i)
        if cell_in_loop is not None and down_cell_in_loop is not None:
          if abs(cell_in_loop-down_cell_in_loop) == 1:
            isInLoop = False if isInLoop else True
        elif cell_in_loop is None and isInLoop:
          count += 1
  return count

if __name__ == "__main__":
  f = open("in_10.txt", "r")
  lines = f.readlines()
  lines = [line.strip() for line in lines]
  f.close()

  print(f"Task 1 solution: {task_1(lines)}")
  print(f"Task 2 solution: {task_2(lines)}")