import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class PartSpec extends AnyFlatSpec with should.Matchers {

  "Creating a part from string" should "give the right Part" in {
    val p = Part(3,4,5,6)
    Part("{x=3,m=4,a=5,s=6}") shouldEqual p
  }

}
