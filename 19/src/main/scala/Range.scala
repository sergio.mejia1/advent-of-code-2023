/** 4-D range for all four types of categories. Its size it's the possible combinations of the 4-D space
  * @param xRange
  *   Range for x category
  * @param mRange
  *   Range for m category
  * @param aRange
  *   Range for a category
  * @param sRange
  *   Range for s category
  */
case class FourDRange(xRange: Range, mRange: Range, aRange: Range, sRange: Range) {
  val size = xRange.size * mRange.size * aRange.size * sRange.size

  /** Slices a 4-D Range along a single axis, generating two 4-D spaces
    * @param category
    *   Axis to be sliced
    * @param cond
    *   Sets onto which range does the boundary value falls. "<" sets it to the right range, ">" sets it to the left
    * @param value
    *   Boundary value of the axis
    * @return
    *   A tuple containing (left, right) ranges (left of right are to be meant as lower to higher value into the chosen
    *   axis)
    */
  def slice(category: String, cond: String, value: Long): (FourDRange, FourDRange) = {
    cond match {
      case "<" =>
        category match {
          case "x" =>
            copy(xRange = Range(xRange.start, value, category)) ->
              copy(xRange = Range(value, xRange.end, category))
          case "m" =>
            copy(mRange = Range(mRange.start, value, category)) ->
              copy(mRange = Range(value, mRange.end, category))
          case "a" =>
            copy(aRange = Range(aRange.start, value, category)) ->
              copy(aRange = Range(value, aRange.end, category))
          case "s" =>
            copy(sRange = Range(sRange.start, value, category)) ->
              copy(sRange = Range(value, sRange.end, category))
        }
      case ">" =>
        category match {
          case "x" =>
            copy(xRange = Range(xRange.start, value + 1, category)) ->
              copy(xRange = Range(value + 1, xRange.end, category))
          case "m" =>
            copy(mRange = Range(mRange.start, value + 1, category)) ->
              copy(mRange = Range(value + 1, mRange.end, category))
          case "a" =>
            copy(aRange = Range(aRange.start, value + 1, category)) ->
              copy(aRange = Range(value + 1, aRange.end, category))
          case "s" =>
            copy(sRange = Range(sRange.start, value + 1, category)) ->
              copy(sRange = Range(value + 1, sRange.end, category))
        }
    }
  }

  override def toString: String = s"{$xRange, $mRange, $aRange, $sRange}"
}

object FourDRange {
  def squaredSpace(start: Long, end: Long): FourDRange = {
    new FourDRange(
      xRange = Range(start, end, "x"),
      mRange = Range(start, end, "m"),
      aRange = Range(start, end, "a"),
      sRange = Range(start, end, "s")
    )
  }
}

/** Exclusive ranges [start, end) belonging to a certain category
  * @param start
  *   Start of range, inclusive
  * @param end
  *   End of range, exclusive
  * @param category
  *   x, m, a or s
  */
case class Range(start: Long, end: Long, category: String) {
  val size = end - start

  override def toString: String = s"$category=[$start, $end]"
}
