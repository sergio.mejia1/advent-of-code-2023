case class Part(x: Long, m: Long, a: Long, s: Long)

object Part {
  def apply(line: String): Part = {
    val matches = "\\{x=(\\d+),m=(\\d+),a=(\\d+),s=(\\d+)}".r.findFirstMatchIn(line).get
    Part(matches.group(1).toLong, matches.group(2).toLong, matches.group(3).toLong, matches.group(4).toLong)
  }
}
