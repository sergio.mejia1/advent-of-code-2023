import scala.util.Try

object Task2Solver extends Solver {
  override def solve(input: List[String]): Either[Throwable, String] = Try {
    val idx = input.indexOf("")
    if (idx != -1) {
      val workflows = input.slice(0, idx)

      val work = workflows.map(Workflow(_)).map(w => w.name -> w).toMap
      evaluate(work, FourDRange.squaredSpace(1, 4001), "in")
        .map(_.size)
        .sum
        .toString
    } else {
      throw new RuntimeException("Empty line not found")
    }
  }.toEither

  def evaluate(workflows: Map[String, Workflow], range: FourDRange, name: String): List[FourDRange] = {
    val workflow = workflows(name)
    val ranges   = workflow.evalRange(range)
    println(ranges)
    ranges.flatMap { case (nextName, range) =>
      nextName match {
        case Some(value) => evaluate(workflows, range, value)
        case None        => List(range)
      }
    }
  }
}
