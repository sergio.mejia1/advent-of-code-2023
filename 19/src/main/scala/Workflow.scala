case class Workflow(name: String, conditions: Vector[Condition]) {
  def eval(part: Part): Either[Outcome, String] = {
    conditions
      .foldLeft[Option[Either[Outcome, String]]](None) { case (res, condition) =>
        res match {
          case None    => condition.eval(part)
          case Some(_) => res
        }
      }
      .get
  }

  def evalRange(range: FourDRange): List[(Option[String], FourDRange)] = {
    conditions.foldLeft(List[(Option[String], FourDRange)]((Some(name) -> range))) { case (list, condition) =>
      list.flatMap { case (optName, curRange) =>
        optName match {
          case Some(evalName) if evalName == name =>
            condition match {
              case e: CompareCondition           =>
                if (e.compareStart(curRange) && e.compareEnd(curRange)) {
                  filterRejected(e.result, curRange).toList
                } else if (e.compareStart(curRange)) {
                  val sliced = curRange.slice(e.category, e.cond, e.value)
                  filterRejected(e.result, sliced._1).toList ++ List(optName -> sliced._2)
                } else if (e.compareEnd(curRange)) {
                  val sliced = curRange.slice(e.category, e.cond, e.value)
                  List(optName -> sliced._1) ++ filterRejected(e.result, sliced._2).toList
                } else List(optName -> curRange)
              case NopOutcomeCondition(Accepted) => List(None -> curRange)
              case NopOutcomeCondition(Rejected) => List.empty
              case NopShiftCondition(name)       => List(Some(name) -> curRange)
              case _                             => List(optName -> curRange)
            }
          case e                                  => List(e -> curRange)
        }
      }
    }
  }

  private def filterRejected(
      outcome: Either[Outcome, String],
      range: FourDRange
    ): Option[(Option[String], FourDRange)] =
    outcome match {
      case Left(Accepted)      => Some(None, range)
      case Left(Rejected)      => None
      case Right(nextWorkflow) => Some(Some(nextWorkflow), range)
    }
}

object Workflow {
  def apply(line: String): Workflow = {
    val matches = "(\\w+)\\{(.*)}".r.findFirstMatchIn(line).get
    new Workflow(matches.group(1), matches.group(2).split(",").map(Condition(_)).toVector)
  }
}
