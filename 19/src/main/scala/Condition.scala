sealed trait Condition {
  def eval(part: Part): Option[Either[Outcome, String]]
}

case class CompareCondition(category: String, cond: String, value: Long, result: Either[Outcome, String])
    extends Condition {
  override def eval(part: Part): Option[Either[Outcome, String]] = {
    val v = category match {
      case "x" => compare(part.x)
      case "m" => compare(part.m)
      case "a" => compare(part.a)
      case "s" => compare(part.s)
    }
    if (v) {
      Some(result)
    } else {
      None
    }
  }

  def compareStart(range: FourDRange): Boolean = {
    category match {
      case "x" => compare(range.xRange.start)
      case "m" => compare(range.mRange.start)
      case "a" => compare(range.aRange.start)
      case "s" => compare(range.sRange.start)
    }
  }

  def compareEnd(range: FourDRange): Boolean = {
    category match {
      case "x" => compare(range.xRange.end)
      case "m" => compare(range.mRange.end)
      case "a" => compare(range.aRange.end)
      case "s" => compare(range.sRange.end)
    }
  }

  def compare(other: Long): Boolean = {
    cond match {
      case ">" => other > value
      case "<" => other < value
    }
  }
}

object CompareCondition {
  def apply(instr: String): CompareCondition = {
    instr match {
      case s"$cat>$value:$outcome" => CompareCondition(cat, ">", value.toLong, getOutcome(outcome))
      case s"$cat<$value:$outcome" => CompareCondition(cat, "<", value.toLong, getOutcome(outcome))
    }
  }

  private def getOutcome(outcome: String): Either[Outcome, String] = outcome match {
    case "A" => Left(Accepted)
    case "R" => Left(Rejected)
    case str => Right(str)
  }
}

case class NopOutcomeCondition(result: Outcome) extends Condition {
  override def eval(part: Part): Option[Either[Outcome, String]] = Some(Left(result))
}

case class NopShiftCondition(name: String) extends Condition {
  override def eval(part: Part): Option[Either[Outcome, String]] = Some(Right(name))
}

object Condition {
  def apply(instruction: String): Condition = {
    val idx = instruction.indexOf(":")
    if (idx != -1) {
      CompareCondition(instruction)
    } else {
      instruction match {
        case "A" => NopOutcomeCondition(Accepted)
        case "R" => NopOutcomeCondition(Rejected)
        case str => NopShiftCondition(str)
      }
    }
  }
}

sealed trait Outcome

object Accepted extends Outcome
object Rejected extends Outcome
