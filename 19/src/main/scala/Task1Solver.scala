import scala.annotation.tailrec
import scala.util.Try
object Task1Solver extends Solver {
  override def solve(input: List[String]): Either[Throwable, String] = Try {
    val idx = input.indexOf("")
    if (idx != -1) {
      val workflows = input.slice(0, idx)
      val parts     = input.slice(idx + 1, input.size)

      val work = workflows.map(Workflow(_)).map(w => w.name -> w).toMap
      parts
        .map(Part(_))
        .filter { p =>
          evaluate(work, p, "in") match {
            case Some(Accepted) => true
            case _              => false
          }
        }
        .foldLeft(0L) { case (accum, p) => accum + p.x + p.m + p.a + p.s }
        .toString
    } else {
      throw new RuntimeException("Empty line not found")
    }
  }.toEither

  @tailrec
  def evaluate(workflows: Map[String, Workflow], part: Part, name: String): Option[Outcome] = {
    val workflow = workflows(name)
    workflow.eval(part) match {
      case Left(outcome) => Some(outcome)
      case Right(next)   => evaluate(workflows, part, next)
    }
  }
}
