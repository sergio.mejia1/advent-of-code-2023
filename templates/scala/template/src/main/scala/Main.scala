import scala.util.{Try, Using, Success, Failure}
import scala.io.Source

object Main {
  def main(args: Array[String]): Unit = {
    val tryLines: Try[List[String]] = Using(Source.fromFile("in_{{day}}.txt")) { source =>
      source.getLines.toList
    }

    tryLines match {
      case Success(lines) => solve(lines)
      case Failure(th)    => th.printStackTrace()
    }
  }

  /* override */
  def solve(input: List[String]): Either[Throwable, String] = {
    println(input)
    Left(new UnsupportedOperationException("Not implemented!"))
  }
}
