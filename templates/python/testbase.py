import unittest
from day_{{day}} import day_{{day}}


class Day{{day}}(unittest.TestCase):
  def test_task_1(self):
    lines = []
    self.assertEqual(day_{{day}}.task_1(lines), 0)

  
  def test_task_2(self):
    lines = []
    self.assertEqual(day_{{day}}.task_2(lines), 0)
  

if __name__ == '__main__':
    unittest.main()