
def check_reflection(map, n_smudges = 0, verbose = False):
  i = 1
  while i < len(map):
    diff = 0
    mirrored = True
    left_n_smudges = n_smudges
    while (i - diff-1) >= 0 and (i + diff) < len(map):
      if verbose:
        print(i, diff, left_n_smudges, map[i+diff-1], map[i-diff])
      if map[i+diff] != map[i-diff-1]:
        diffs = [map[i+diff][n] != map[i-diff-1][n] for n in range(len(map[0]))].count(True)
        if diffs <= left_n_smudges:
          left_n_smudges -= diffs
        else:
          mirrored = False
          break
      diff += 1
    if mirrored and left_n_smudges == 0:
      return i
    else:
      i += 1
  return None

def build_transpose(map):
  return ["".join([map[j][i] for j in range(len(map))]) for i in range(len(map[0]))]

def task_1(maps, n_smudges = 0, verbose = False):
  sum = 0
  for map in maps:
    n = check_reflection(map, n_smudges=n_smudges, verbose=verbose)
    if n is not None:
      sum += 100*n
      if verbose:
        print("\n".join(map), n, "ROW")
    else:
      tr = build_transpose(map)
      n = check_reflection(tr, n_smudges=n_smudges, verbose=verbose)
      if n is None:
        raise Exception("WARN: NO REFLECTION FOUND")
      sum += n
      if verbose:
        print("\n".join(tr), n, "COL")
    if verbose:
      print("")
  return sum

if __name__ == "__main__":
  f = open("in_13.txt", "r")
  lines = [l.split() for l in f.read().strip().split("\n\n")]
  f.close()

  print(f"Solution to Task 1: {task_1(lines)}")
  print(f"Solution to Task 2: {task_1(lines, n_smudges=1)}")
