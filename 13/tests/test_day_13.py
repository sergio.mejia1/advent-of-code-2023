import unittest
from day_13 import day_13


class Day13(unittest.TestCase):
  
  
  def test_task_1(self):
    lines = [[
      "#.##..##.",
      "..#.##.#.",
      "##......#",
      "##......#",
      "..#.##.#.",
      "..##..##.",
      "#.#.##.#.",
    ],
    [
      "#...##..#",
      "#....#..#",
      "..##..###",
      "#####.##.",
      "#####.##.",
      "..##..###",
      "#....#..#",
    ]]
    self.assertEqual(day_13.task_1(lines), 405)
  

  def test_a_weird_result(self):
    lines = [[
      "....#..#.",
      "####....#",
      ".##......",
      "#.##.##.#",
      "#..##..##",
      "#..##..##",
      "#####..## ",
    ]]
    self.assertEqual(day_13.task_1(lines), 6)
  
  
  def test_task_2(self):
    lines = [[
      "#.##..##.",
      "..#.##.#.",
      "##......#",
      "##......#",
      "..#.##.#.",
      "..##..##.",
      "#.#.##.#.",
    ],
    [
      "#...##..#",
      "#....#..#",
      "..##..###",
      "#####.##.",
      "#####.##.",
      "..##..###",
      "#....#..#",
    ]]
    self.assertEqual(day_13.task_1(lines, n_smudges=1), 400)
  

if __name__ == '__main__':
    unittest.main()