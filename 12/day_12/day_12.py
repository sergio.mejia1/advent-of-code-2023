import itertools
import re
import functools

class SpringLine:
  def __init__(self, line, expand = 1) -> None:
    self.input = list(line.split()[0])
    self.input = ((self.input + ["?"]) * expand)[:-1]

    self.sets = [int(s) for s in line.split()[1].split(",")] * expand

  def __repr__(self) -> str:
    return f"{''.join(self.input)} -> {self.sets}"
  
  def possible_combinations(self) -> int:
    count = 0
    for product in itertools.product([True, False], repeat=self.input.count("?")):
      l = [*self.input]
      j = 0
      for i, val in enumerate(l):
        if val == "?":
          l[i] = "#" if product[j] else "."
          j += 1
      if self.valid(l):
        count += 1
    return count
      
  def valid(self, input):
    groups = re.findall("#+", "".join(input))
    return self.sets == [len(group) for group in groups]
  
  def possible_combinations_rec(self) -> int:
    return self.possible_combinations_aux("".join(self.input), tuple(self.sets))
  
  @functools.cache
  def possible_combinations_aux(self, input, sets) -> int:
    # Check if we finished

    if not sets:
      if "#" in input:
        return 0 # We still have springs but we finished the sets, bad result
      else:
        return 1 # Emptied input, no more springs, this is a valid solution
    if not input:
      return 0 # We have sets but no input, bad result

    next_character = input[0]
    next_group = sets[0]

    def treat_spring():
      next_possible_group = input[:next_group]
      next_possible_group = next_possible_group.replace("?", "#")
      if next_possible_group.count("#") != next_group:
        return 0 # We have a dot somewhere so this is not a group

      if len(input) == next_group and len(sets) == 1:
        return 1 # We have exhausted the input and just this good group remains
      
      # Check character after found group, it can't be a spring, because it would make a bigger set
      if next_group < len(input) and input[next_group] not in "#":
        return self.possible_combinations_aux(input[next_group+1:], sets[1:])
      
      return 0
    
    def treat_space():
      return self.possible_combinations_aux(input[1:], sets)

    def treat_unknown():
      return treat_spring() + treat_space()
    
    if next_character == "#":
      return treat_spring()
    elif next_character == ".":
      return treat_space()
    elif next_character == "?":
      return treat_unknown()



def task_1(lines):
  input = [SpringLine(line) for line in lines]
  return sum([line.possible_combinations() for line in input])

def task_2(lines, expand = 5):
  input = [SpringLine(line, expand) for line in lines]
  return sum([line.possible_combinations_rec() for line in input])

if __name__ == "__main__":
  f = open("in_12.txt", "r")
  lines = f.readlines()
  lines = [line.strip() for line in lines]
  f.close()

  # Analysis. The max ? count is 18, which is 2**18 ~ 1e5 which is ugly but we should be ok for part 1
  print(max([line.count("?") for line in lines]))
  #print(f"Solution to Task 1: {task_1(lines)}")
  print(f"Solution to Task 1 rec: {task_2(lines, expand=1)}")

  # Task 2 makes n*5+5 which in turn is 2**(18*5+5) ~ 1e28... Nope, forget about brute force
  print(f"Solution to Task 2: {task_2(lines)}")
