import scala.util.{Failure, Success, Try, Using}
import scala.io.Source

object Main {
  def main(args: Array[String]): Unit = {
    val tryLines: Try[List[String]] = Using(Source.fromFile("in_02.txt")) { source =>
      source.getLines.toList
    }

    tryLines match {
      case Success(lines) =>
        println(Task1Solver.solve(lines))
        println(Task2Solver.solve(lines))
      case Failure(th)    => th.printStackTrace()
    }
  }

}
