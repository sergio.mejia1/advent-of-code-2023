object Task1Solver extends Solver {
  override def solve(input: List[String]): Either[Throwable, String] = {
    val testGameSet = new GameSet(12, 13, 14)
    val games       = input.map(Game(_))
    Right(
      games
        .filter(_.exists(testGameSet))
        .foldRight(0)((g, sum) => sum + g.gameId)
        .toString
    )
  }
}
