case class Game(gameId: Int, sets: List[GameSet]) {
  def exists(gameSet: GameSet): Boolean = sets.forall(_.contains(gameSet))

  val maxSet: GameSet = {
    new GameSet(red = sets.map(_.red).max, green = sets.map(_.green).max, blue = sets.map(_.blue).max)
  }
}

object Game                                         {
  /*
    Line in the form "Game N: GameSet; GameSet...
   */
  def apply(line: String): Game = {
    val split = "^Game (\\d+): (.*)$".r.findFirstMatchIn(line).get
    new Game(split.group(1).toInt, split.group(2).split(";").map(GameSet(_)).toList)
  }
}
case class GameSet(red: Int, green: Int, blue: Int) {
  def contains(gameSet: GameSet): Boolean = gameSet.red >= red && gameSet.blue >= blue && gameSet.green >= green
}

object GameSet {
  /*
  Line in the form "X Y, X Y,..." where X is an int and Y is red|blue|green
   */
  def apply(line: String): GameSet = {
    val values                                           = line.trim.split("[, ]")
    def valueOf(list: Array[String], color: String): Int = {
      val idx = list.indexOf(color)
      if (idx == -1) 0
      else list(idx - 1).toInt
    }
    new GameSet(valueOf(values, "red"), valueOf(values, "green"), valueOf(values, "blue"))
  }
}
