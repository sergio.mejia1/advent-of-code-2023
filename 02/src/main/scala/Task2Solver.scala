object Task2Solver extends Solver {
  override def solve(input: List[String]): Either[Throwable, String] = {
    val games = input.map(Game(_))
    Right(
      games
        .map(_.maxSet)
        .map(set => set.red * set.green * set.blue)
        .sum
        .toString
    )
  }
}
