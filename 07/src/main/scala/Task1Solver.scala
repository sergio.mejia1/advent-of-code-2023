import scala.util.Try

object Task1Solver extends Solver {
  override def solve(input: List[String]): Either[Throwable, String] = Try {
    val sorted = input.map { str =>
      val values = str.split(" ")
      new CardHand(values(0).split("").toList, values(1).toLong)
    }.sorted
    sorted.zipWithIndex
      .map { case (hand, i) =>
        hand.bid * (sorted.size - i)
      }
      .sum
      .toString
  }.toEither
}
