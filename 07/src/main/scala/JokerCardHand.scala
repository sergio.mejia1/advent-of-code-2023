import scala.util.Try

case class JokerCardHand(override val cards: List[String], override val bid: Long) extends CardHand(cards, bid) {

  private val maxCard: Option[(String, Int)] = Try {
    cards
      .groupBy(identity)
      .filter { case (key, _) => key != "J" }
      .map { case (number, list) => number -> list.size }
      .toList
      .maxBy(_._2)
  }.toOption

  override val cardsPerNumber: Map[String, Int] = {
    val tempCardsPerNumber = cards.groupBy(identity).map { case (number, list) => number -> list.size }
    maxCard match {
      case Some(card) =>
        tempCardsPerNumber
          .updated(card._1, tempCardsPerNumber.getOrElse("J", 0) + card._2)
          .updated("J", 0) // We've used the jokers, remove them
      case None       => tempCardsPerNumber
    }
  }

  override def toInt(card: String): Int = {
    card match {
      case "A" => 13
      case "K" => 12
      case "Q" => 11
      case "T" => 9
      case "9" => 8
      case "8" => 7
      case "7" => 6
      case "6" => 5
      case "5" => 4
      case "4" => 3
      case "3" => 2
      case "2" => 1
      case "J" => 0
    }
  }

  override def toString: String = s"JokerCardHand($cards,$bid)"
}
