class CardHand(val cards: List[String], val bid: Long) extends Ordered[CardHand] {
  val cardsPerNumber: Map[String, Int] = cards.groupBy(identity).map { case (number, list) => number -> list.size }

  def isFiveOfAKind: Boolean  = cardsPerNumber.exists { case (_, count) => count == 5 }
  def isFourOfAKind: Boolean  = cardsPerNumber.exists { case (_, count) => count == 4 }
  def isThreeOfAKind: Boolean = cardsPerNumber.exists { case (_, count) => count == 3 }
  def isTwoOfAKind: Boolean   = cardsPerNumber.exists { case (_, count) => count == 2 }
  def isTwoPair: Boolean      = cardsPerNumber.count { case (_, count) => count == 2 } == 2
  def isFullHouse: Boolean    = isThreeOfAKind && isTwoOfAKind

  def strength: Int = {
    if (isFiveOfAKind) 6
    else if (isFourOfAKind) 5
    else if (isFullHouse) 4
    else if (isThreeOfAKind) 3
    else if (isTwoPair) 2
    else if (isTwoOfAKind) 1
    else 0
  }

  import scala.math.Ordered.orderingToOrdered
  override def compare(that: CardHand): Int = {
    val diffStrength = that.strength - strength
    if (diffStrength == 0) {
      that.cards
        .zip(cards)
        .map { case (str, str1) => toInt(str) - toInt(str1) }
        .find(n => n != 0)
        .getOrElse(0)
    } else diffStrength
  }

  def toInt(card: String): Int = {
    card match {
      case "A" => 13
      case "K" => 12
      case "Q" => 11
      case "J" => 10
      case "T" => 9
      case "9" => 8
      case "8" => 7
      case "7" => 6
      case "6" => 5
      case "5" => 4
      case "4" => 3
      case "3" => 2
      case "2" => 1
    }
  }

  override def toString: String = s"CardHand($cards,$bid)"
}
