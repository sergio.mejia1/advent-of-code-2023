import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class Task1SolverSpec extends AnyFlatSpec with should.Matchers {

  "Task 1" should "give the correct answer" in {
    val lines = List(
      "32T3K 765",
      "T55J5 684",
      "KK677 28",
      "KTJJT 220",
      "QQQJA 483"
    )

    Task1Solver.solve(lines) match {
      case Left(error)  =>
        error.printStackTrace()
        fail()
      case Right(value) => value shouldEqual "6440"
    }
  }
}
