import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class JokerCardHandSpec extends AnyFlatSpec with should.Matchers {
  "Used jokers" should "not be available anymore" in {
    JokerCardHand(List("J", "3", "Q", "J", "2"), 709).strength shouldEqual 3
    JokerCardHand(List("J", "J", "4", "Q", "K"), 711).strength shouldEqual 3
  }
}
