import unittest
from day_03 import day_03


class Day01(unittest.TestCase):
  def test_task_1(self):
    lines = [
      "467..114..",
      "...*......",
      "..35..633.",
      "......#...",
      "617*......",
      ".....+.58.",
      "..592.....",
      "......755.",
      "...$.*....",
      ".664.598.."
    ]
    self.assertEqual(day_03.task_1(lines), 4361)

  
  def test_task_2(self):
    lines = [
      "467..114..",
      "...*......",
      "..35..633.",
      "......#...",
      "617*......",
      ".....+.58.",
      "..592.....",
      "......755.",
      "...$.*....",
      ".664.598.."
    ]
    self.assertEqual(day_03.task_2(lines), 467835)
  

if __name__ == '__main__':
    unittest.main()