def nToXY(n, length):
  return (n % length, n // length)

def checkForSymbol(lines, j, xStart, xEnd):
  dirs = [(x,j-1) for x in range(xStart-1, xEnd+1)] + \
    [(x,j+1) for x in range(xStart-1, xEnd+1)] + [(xStart-1,j), (xEnd, j)]
  X, Y = len(lines[0]), len(lines)
  for i, j in dirs:
    if 0 <= i < X and 0 <= j < Y:
      #print(f"({i}, {j}) = {lines[j][i]}", end=", ")
      if lines[j][i] not in digits and lines[j][i] != '.':
        return True
  print("")
  return False

digits = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]

def task_1(lines):
  """ Use lines as a 2D-array"""
  n = 0
  X, Y = len(lines[0]), len(lines)
  print(X, Y)
  sum = 0
  while n < X*Y:
    i, j = nToXY(n,X)
    if lines[j][i] in digits:
      print(f"Found a starting number in ({i}, {j}) = {lines[j][i]}")
      nStart, j = nToXY(n,X)
      nEnd, _ = nToXY(n,X)  
      while nEnd < X-1 and lines[j][nEnd] in digits:
        n += 1
        nEnd, _ = nToXY(n,X)
      if nEnd == X-1:
        if lines[j][nEnd] in digits:
          nEnd += 1
      num = int(lines[j][nStart:nEnd])
      print(f"Number ends in ({nEnd}, {j}) and is {num}")
      if checkForSymbol(lines, j, nStart, nEnd):
        print(f"num is part")
        sum += num
    n += 1
  return sum

def gear_ratio(lines, J, I):
  X, Y = len(lines[0]), len(lines)
  searched = {(I-1,J-1): False, (I,J-1): False, (I+1,J-1): False,
              (I-1,J  ): False, (I,J  ): False, (I+1,J  ): False,
              (I-1,J+1): False, (I,J+1): False, (I+1,J+1): False}
  nums = []
  for search in searched:
    if not searched[search]:
      i, j = search
      if lines[j][i] in digits:
        iStart = iEnd = i
        while iStart >= 0 and lines[j][iStart] in digits:
          if (iStart, j) in searched:
            searched[(iStart, j)] = True
          iStart -= 1
        iStart += 1
        while iEnd < X and lines[j][iEnd] in digits:
          if (iEnd, j) in searched:
            searched[(iEnd, j)] = True
          iEnd += 1
        num = int(lines[j][iStart:iEnd])
        nums.append(num)
  if len(nums) == 2:
    return nums[0] * nums[1]
  else:
    return None

def task_2(lines):
  X, Y = len(lines[0]), len(lines)
  sum = 0
  for j in range(Y):
    for i in range(X):
      if lines[j][i] == '*':
        ratio = gear_ratio(lines, j, i)
        if ratio is not None:
          sum += ratio
  return sum

if __name__ == '__main__':
  f = open("in_03.txt", "r")
  lines = f.readlines()
  lines = [line.strip() for line in lines]

  print(task_1(lines))
  print(task_2(lines))

  f.close()