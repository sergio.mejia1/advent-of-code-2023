#!/bin/bash

day=$(date +%d)
if [ $# -ne 0 ] 
  then
    day=$1
fi

cp templates/scala/template/ $day/ -r
cd $day
find . -type f -name '*' | xargs sed -i "s/{{day}}/$day/"

cd ..
AOC_SESSION_COOKIE=$(<.credentials)
echo Downloading from https://adventofcode.com/2023/day/$((10#$day))/input
curl --cookie "session=${AOC_SESSION_COOKIE}" https://adventofcode.com/2023/day/$((10#$day))/input -o $day/in_$day.txt --create-dirs
