import unittest
from day_17 import day_17


class Day17(unittest.TestCase):

  inputMap = [
    "2413432311323",
    "3215453535623",
    "3255245654254",
    "3446585845452",
    "4546657867536",
    "1438598798454",
    "4457876987766",
    "3637877979653",
    "4654967986887",
    "4564679986453",
    "1224686865563",
    "2546548887735",
    "4322674655533",
  ]
  
  def test_task_1(self):
    lines = [[int(c) for c in line] for line in self.inputMap]
    self.assertEqual(day_17.task_1(lines), 102)
  

  
  def test_task_2(self):
    lines = [[int(c) for c in line] for line in self.inputMap]
    self.assertEqual(day_17.task_2(lines), 94)
  
  
  

if __name__ == '__main__':
    unittest.main()