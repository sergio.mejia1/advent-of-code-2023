import heapq
import time
import os

# Cost = int
# Father = (x, y, dir, StraightCost)
# Son = (x, y, dir, StraightCost)
def build_node(coord, dir, straightCost):
  return (coord[0], coord[1], dir, straightCost)


dirs = {"north": (-1, 0), "east": (0, 1), "south": (1, 0), "west": (0, -1)}
def get_valid_children(matrix, lowest, minMove, maxMove):
  children = []
  for name, dir in dirs.items():
    x, y, dirName, straightCost = lowest
    if 0 <= x+dir[0] < len(matrix) and 0 <= y+dir[1] < len(matrix[0]):
      if name == dirName:
        if straightCost < maxMove:
          children.append((x+dir[0], y+dir[1], name, straightCost+1))
      elif dirName == "north" and name != "south" or \
            dirName == "east" and name != "west" or \
            dirName == "west" and name != "east" or \
            dirName == "south" and name != "north": 
        if 0 <= x+minMove*dir[0] < len(matrix) and 0 <= y+minMove*dir[1] < len(matrix[0]):
          children.append((x+minMove*dir[0], y+minMove*dir[1], name, minMove))
  #print(lowest, "->", children)
  return children


def get_ultra_valid_children(matrix, lowest):
  children = []
  for name, dir in dirs.items():
    x, y, dirName, straightCost = lowest
    if 0 <= x+dir[0] < len(matrix) and 0 <= y+dir[1] < len(matrix[0]):
      if name == dirName and straightCost < 10:
          children.append((x+dir[0], y+dir[1], name, straightCost+1))
      elif 0 <= x+4*dir[0] < len(matrix) and 0 <= y+4*dir[1] < len(matrix[0]):
        children.append((x+4*dir[0], y+4*dir[1], name, 4))
  # print(lowest, "->", children)
  return children

def cost(matrix, origin, dest):
  #print("Origindest:",origin, dest)
  s = 0
  #print("X", min(dest[0], origin[0])+1, max(dest[0], origin[0])+1)
  #print("Y", min(dest[1], origin[1])+1, max(dest[0], origin[0])+1)
  deltaX = 1 if dest[0] > origin[0] else -1
  deltaY = 1 if dest[1] > origin[1] else -1
  for x in range(origin[0], dest[0], deltaX):
    s += matrix[x][dest[1]]
  for y in range(origin[1], dest[1], deltaY):
    s += matrix[dest[0]][y]
  return s


def dijkstra4D(matrix, start, minMove, maxMove):
  # quads = [(x,y,dir,n) for n in range(4,11) for dir in dirs for y in range(len(matrix[0])) for x in range(len(matrix))]
  visited = {}
  # print(visited)
  p_queue = [(0, build_node(start, "east", 0), build_node(start, "east", 0))]
  ret = {}
  #quad: (-1,-1,9e9) for quad in quads
  # sorted(p_queue, key=sort_func)
  while len(p_queue) > 0:
    #p_queue.sort(key=sort_func)
    lowest = heapq.heappop(p_queue)
    if lowest[2] not in visited:
      visited[lowest[2]] = True
      children = get_valid_children(matrix, lowest[2], minMove, maxMove)
      for child in children:
        #print(child)
        new_cost = lowest[0] + cost(matrix, lowest[2], child)#matrix[child[0]][child[1]]
        heapq.heappush(p_queue, (new_cost, lowest[2], child))
      if lowest[2] not in ret or lowest[0] < ret[lowest[2]][-1]:
        ret[lowest[2]] = (*lowest[1], lowest[0])
  #print(ret)
  return ret

def get_shortest_path(matrix, a_dijkstra, minMove, maxMove):
  start_coord = (0,0)
  end_coord = (len(matrix)-1, len(matrix[0])-1)
  bt = a_dijkstra(matrix, start_coord, minMove, maxMove)
  lk = None
  min = 9e9
  for dir in dirs:
    for n in range(minMove,maxMove):
      if (end_coord[0],end_coord[1],dir,n) in bt and bt[(end_coord[0],end_coord[1],dir,n)][-1] < min:
        min = bt[(end_coord[0],end_coord[1],dir,n)][-1]
        lk = (end_coord[0],end_coord[1],dir,n,min)
  # Backtracking
  ret = []
  cop_end = lk
  while bt[cop_end[:-1]] != cop_end:
    ret.append(cop_end[:2])
    cop_end = bt[cop_end[:-1]]
  return ret

def manhattan(p1, p2):
  return abs(p1[1]-p2[1]) + abs(p1[0]-p2[0])

def print_path(path, x, y, map = None):
  print(path)
  for j in range(x):
    for i in range(y):
      if (j,i) not in path:
        print(".", end='')
      else:
        idx = path.index((j,i))
        if idx > 0:
          if map is not None:
            print(map[j][i], end='')
          elif path[idx][0] < path[idx-1][0]:
            print("v", end='')
          elif path[idx][0] > path[idx-1][0]:
            print("^", end='')
          elif path[idx][1] < path[idx-1][1]:
            print(">", end='')
          elif path[idx][1] > path[idx-1][1]:
            print("<", end='')
        else:
          print(".", end='')
    print("")

def print_bt(bt, path=None):
  for j, row in enumerate(bt):
    for i, item in enumerate(row):
      if (item[0], item[1]) == (-1,-1) or (path is not None and (j,i) not in path):
        print(".", end='')
      else:
        if item[0] < j:
          print("v", end='')
        elif item[0] > j:
          print("^", end='')
        elif item[1] < i:
          print(">", end='')
        elif item[1] > i:
          print("<", end='')
    print("")

def a_star(matrix):
  start = (0,0)
  end = (len(matrix)-1, len(matrix[0])-1)
  p_queue = [(0, build_node(start, "east", 0))]
  bt = [[(-1, -1) for _ in range(len(matrix[0]))] for _ in range(len(matrix))]
  gScore = [[9e9 for _ in range(len(matrix[0]))] for _ in range(len(matrix))]
  fScore = [[9e9 for _ in range(len(matrix[0]))] for _ in range(len(matrix))]
  gScore[start[0]][start[1]] = 0
  fScore[start[0]][start[1]] = manhattan(start, end)

  while len(p_queue) > 0:
    lowest = heapq.heappop(p_queue)
    x, y, _, _ = lowest[1]
    if (x,y) == end:
      ret = []
      cop_end = end
      while bt[cop_end[0]][cop_end[1]] != cop_end and bt[cop_end[0]][cop_end[1]] != (-1,-1):
        ret.append(cop_end)
        cop_end = bt[cop_end[0]][cop_end[1]]
      ret.append(start)
      print("")
      print_bt(bt, ret)
      return ret
    
    children = get_valid_children(matrix, lowest[1])
    for child in children:
      xC, yC, _, _ = child
      cost = gScore[x][y] + matrix[xC][yC]
      if cost < gScore[xC][yC]:
        bt[xC][yC] = (x,y)
        gScore[xC][yC] = cost
        fScore[xC][yC] = cost + manhattan((xC, yC), end)
        if child not in p_queue:
          heapq.heappush(p_queue, (fScore[xC][yC], child))
    #time.sleep(0.1)
    os.system("cls")
    print_bt(bt)



def task_1(lines):
  ret = get_shortest_path(lines, dijkstra4D, 1, 3)
  sum = 0
  print_path(ret, len(lines), len(lines[0]), lines)
  for c in ret[:-1]:
    sum += lines[c[0]][c[1]]
  return sum

def task_2(lines):
  ret = get_shortest_path(lines, dijkstra4D, 4, 10)
  sum = 0
  print_path(ret, len(lines), len(lines[0]))
  for c in range(len(ret)-1):
    heat = cost(lines, ret[c], ret[c+1])
    print(ret[c], "=", lines[ret[c][0]][ret[c][1]], ret[c+1], "=",  lines[ret[c+1][0]][ret[c+1][1]], heat)
    sum += heat
  return sum

if __name__ == "__main__":
    f = open("in_17.txt", "r")
    lines = f.readlines()
    lines = [[int(c) for c in line.strip()] for line in lines]

    print(task_1(lines))
    print(task_2(lines))

    f.close()