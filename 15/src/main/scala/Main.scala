import scala.util.{Failure, Success, Try, Using}
import scala.io.Source

object Main {
  def main(args: Array[String]): Unit = {
    val tryLines: Try[List[String]] = Using(Source.fromFile("in_15.txt")) { source =>
      source.getLines.toList
    }

    tryLines match {
      case Success(lines) =>
        Task1Solver.solve(lines) match {
          case Left(error)  => error.printStackTrace()
          case Right(value) => println(value)
        }
        Task2Solver.solve(lines) match {
          case Left(error) => error.printStackTrace()
          case Right(value) => println(value)
        }
      case Failure(th)    => th.printStackTrace()
    }
  }
}
