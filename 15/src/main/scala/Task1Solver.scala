object Task1Solver extends Solver with Hasher {
  override def solve(input: List[String]): Either[Throwable, Long] = {
    input.headOption.toRight(new RuntimeException("Input empty")).map { line =>
      line.split(",").foldLeft(0L) { case (accum, instruction) =>
        accum + HASH(instruction)
      }
    }
  }
}
