trait Hasher {
  def HASH(input: String): Int =
    input.foldLeft(0) { case (hash, char) =>
      (17 * (hash + char.toInt)) % 256
    }
}
