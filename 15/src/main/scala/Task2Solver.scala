object Task2Solver extends Solver with Hasher {
  override def solve(input: List[String]): Either[Throwable, Long] = {
    input.headOption.toRight(new RuntimeException("Input empty")).map { line =>
      val boxes = line
        .split(",")
        .map(Instruction(_))
        .foldLeft(List.fill(256)(List.empty[Instruction])) { case (list, instr) =>
          val hashIdx = HASH(instr.label)
          instr.action match {
            case "=" =>
              val lensIdx = list(hashIdx).indexWhere(_.label.equals(instr.label))
              if (lensIdx == -1) {
                list.updated(hashIdx, list(hashIdx) :+ instr)
              } else {
                list.updated(hashIdx, list(hashIdx).updated(lensIdx, instr))
              }
            case "-" => list.updated(hashIdx, list(hashIdx).filter(!_.label.equals(instr.label)))
          }
        }
      boxes.zipWithIndex
        .foldLeft(0) { case (accum, (list, i)) =>
          accum + (i + 1) *
            list.zipWithIndex.foldLeft(0) { case (innerAccum, (lens, j)) =>
              innerAccum + (lens.focalLength.get * (j + 1))
            }
        }
    }
  }
}

case class Instruction(label: String, action: String, focalLength: Option[Int])

object Instruction {
  def apply(input: String): Instruction = {
    val eqIdx = input.indexOf("=")
    if (eqIdx != -1) {
      val eqSplit = input.split("=")
      new Instruction(eqSplit(0), "=", Some(eqSplit(1).toInt))
    } else {
      val rmSplit = input.split("-")
      new Instruction(rmSplit(0), "-", None)
    }
  }
}
