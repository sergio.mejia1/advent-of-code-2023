import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class Task2SolverSpec extends AnyFlatSpec with should.Matchers {

  "task 2" should "return the correct answer" in {
    val l = List("rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7")
    Task2Solver.solve(l) shouldEqual Right(145L)
  }

}
