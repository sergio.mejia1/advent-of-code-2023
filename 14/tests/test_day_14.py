import unittest
from day_14 import day_14


class Day14(unittest.TestCase):
  def test_task_1(self):
    lines = [
      "O....#....",
      "O.OO#....#",
      ".....##...",
      "OO.#O....O",
      ".O.....O#.",
      "O.#..O.#.#",
      "..O..#O..O",
      ".......O..",
      "#....###..",
      "#OO..#....",
    ]
    self.assertEqual(day_14.task_1(lines), 136)

  def test_tilt_north(self):
    lines = [
      "O....#....",
      "O.OO#....#",
      ".....##...",
      "OO.#O....O",
      ".O.....O#.",
      "O.#..O.#.#",
      "..O..#O..O",
      ".......O..",
      "#....###..",
      "#OO..#....",
    ]
    expected = [
      "OOOO.#.O..",
      "OO..#....#",
      "OO..O##..O",
      "O..#.OO...",
      "........#.",
      "..#....#.#",
      "..O..#.O.O",
      "..O.......",
      "#....###..",
      "#....#....",
    ]
    self.assertEqual(day_14.tilt_north(lines), [list(l) for l in expected])

  def test_tilt_south(self):
    lines = [
      "O....#....",
      "O.OO#....#",
      ".....##...",
      "OO.#O....O",
      ".O.....O#.",
      "O.#..O.#.#",
      "..O..#O..O",
      ".......O..",
      "#....###..",
      "#OO..#....",
    ]
    expected = [
      ".....#....",
      "....#....#",
      "...O.##...",
      "...#......",
      "O.O....O#O",
      "O.#..O.#.#",
      "O....#....",
      "OO....OO..",
      "#OO..###..",
      "#OO.O#...O",
    ]
    self.assertEqual(day_14.tilt_south(lines), [list(l) for l in expected])

  def test_tilt_east(self):
    lines = [
      "O....#....",
      "O.OO#....#",
      ".....##...",
      "OO.#O....O",
      ".O.....O#.",
      "O.#..O.#.#",
      "..O..#O..O",
      ".......O..",
      "#....###..",
      "#OO..#....",
    ]
    expected = [
      "....O#....",
      ".OOO#....#",
      ".....##...",
      ".OO#....OO",
      "......OO#.",
      ".O#...O#.#",
      "....O#..OO",
      ".........O",
      "#....###..",
      "#..OO#....",
    ]
    self.assertEqual(day_14.tilt_east(lines), [list(l) for l in expected])

  def test_tilt_west(self):
    lines = [
      "O....#....",
      "O.OO#....#",
      ".....##...",
      "OO.#O....O",
      ".O.....O#.",
      "O.#..O.#.#",
      "..O..#O..O",
      ".......O..",
      "#....###..",
      "#OO..#....",
    ]
    expected = [
      "O....#....",
      "OOO.#....#",
      ".....##...",
      "OO.#OO....",
      "OO......#.",
      "O.#O...#.#",
      "O....#OO..",
      "O.........",
      "#....###..",
      "#OO..#....",
    ]
    self.assertEqual(day_14.tilt_west(lines), [list(l) for l in expected])

  
  def test_task_2(self):
    lines = [
      "O....#....",
      "O.OO#....#",
      ".....##...",
      "OO.#O....O",
      ".O.....O#.",
      "O.#..O.#.#",
      "..O..#O..O",
      ".......O..",
      "#....###..",
      "#OO..#....",
    ]
    self.assertEqual(day_14.task_2(lines), 64)
  
  def test_task_2_cheating(self):
    lines = [
      "O....#....",
      "O.OO#....#",
      ".....##...",
      "OO.#O....O",
      ".O.....O#.",
      "O.#..O.#.#",
      "..O..#O..O",
      ".......O..",
      "#....###..",
      "#OO..#....",
    ]
    self.assertEqual(day_14.task_2(lines), 64)
  
  

if __name__ == '__main__':
    unittest.main()