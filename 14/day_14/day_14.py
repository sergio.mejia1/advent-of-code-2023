from copy import deepcopy
import time 
import os
CUBE = "#"
ROCK = "O"

def print_map(map):
  l = "\n".join(["".join(l) for l in map])
  os.system("cls")
  print(f'=======================\n{l}\n=======================\n')


def tilt_north(map):
  new_map = [list(line) for line in map]
  for i in range(len(map[0])):
    next_rock = j = 0
    while j < len(map):
      count_rocks = 0
      while j < len(map) and map[j][i] != CUBE:
        if map[j][i] == ROCK:
          count_rocks += 1
        j += 1
      #print(next_rock,i, count_rocks,j)
      for n in range(count_rocks):
        new_map[next_rock+n][i] = "O"
      for n in range(next_rock+count_rocks, j):
        new_map[n][i] = "."
      j += 1
      next_rock = j
  #print_map(new_map)
  return new_map

def tilt_south(map):
  new_map = [list(line) for line in map]
  for i in range(len(map[0])):
    next_rock = j = len(map)-1
    while 0 <= j:
      count_rocks = 0
      while 0 <= j and map[j][i] != CUBE:
        if map[j][i] == ROCK:
          count_rocks += 1
        j -= 1
      #print(next_rock,i, count_rocks,j)
      for n in range(count_rocks):
        new_map[next_rock-n][i] = "O"
      for n in range(next_rock-count_rocks, j, -1):
        new_map[n][i] = "."
      j -= 1
      next_rock = j
  #print_map(new_map)
  return new_map

def tilt_east(map):
  new_map = [list(line) for line in map]
  for j in range(len(map)):
    next_rock = i = len(map[0])-1
    while 0 <= i:
      count_rocks = 0
      while 0 <= i and map[j][i] != CUBE:
        if map[j][i] == ROCK:
          count_rocks += 1
        i -= 1
      #print(next_rock,i, count_rocks,j)
      for n in range(count_rocks):
        new_map[j][next_rock-n] = "O"
      for n in range(next_rock-count_rocks, i, -1):
        new_map[j][n] = "."
      i -= 1
      next_rock = i
  #print_map(new_map)
  return new_map

def tilt_west(map):
  new_map = [list(line) for line in map]
  for j in range(len(map)):
    next_rock = i = 0
    while i < len(map[0]):
      count_rocks = 0
      while i < len(map[0]) and map[j][i] != CUBE:
        if map[j][i] == ROCK:
          count_rocks += 1
        i += 1
      #print(next_rock,i, count_rocks,j)
      for n in range(count_rocks):
        new_map[j][next_rock+n] = "O"
      for n in range(next_rock+count_rocks, i):
        new_map[j][n] = "."
      i += 1
      next_rock = i
  #print_map(new_map)
  return new_map

def tilt(map, direction):
  if direction == "north":
    return tilt_north(map)
  if direction == "south":
    return tilt_south(map)
  elif direction == "east":
    return tilt_east(map)
  elif direction == "west":
    return tilt_west(map)
  else:
    raise Exception("Direction invalid")

def task_1(map):
  height = len(map)
  accum = 0
  for i in range(len(map[0])):
    next_rock = 0
    j = 0
    while j < len(map):
      count_rocks = 0
      while j < len(map) and map[j][i] != CUBE:
        if map[j][i] == ROCK:
          count_rocks += 1
        j += 1
      accum += sum([(height-n) for n in range(next_rock, next_rock+count_rocks)])
      # print(next_rock, count_rocks, i, accum)
      j += 1
      next_rock = j
  return accum

def compare_maps(map, other_map):
  return "".join(["".join(l) for l in map]) == "".join(["".join(l) for l in other_map])

def task_2(map):
  expansion = 100
  original_order = ["north", "west", "south", "east"]
  order = original_order*expansion
  memo = [[]]*len(order)
  rounds = 1_000_000_000
  # How to identify loop?
  divisor = 0
  i = 0
  cur_map = map
  while i < len(original_order) or not compare_maps(cur_map, memo[i-len(original_order)]):
    print(divisor)
    divisor += 1
    memo[i] = cur_map
    cur_map = tilt(cur_map, order[i])
    i = (i+1) % len(order)
    #time.sleep(1)
  print(divisor)

def weight(map):
  height = len(map)
  sum = 0
  for j in range(len(map)):
    for i in range(len(map[j])):
      if map[j][i] == ROCK:
        sum += (height-j)
  return sum

def task_2_cheating(map, magic_number, magic_loop):
  order = ["north", "west", "south", "east"]
  cur_map = map
  i = 0
  for _ in range(magic_number):
    cur_map = tilt(cur_map, order[i])
    i = (i+1) % len(order)
  rounds = 1_000_000_000 * 4
  mod = (rounds - magic_number) % magic_loop
  for _ in range(mod):
    cur_map = tilt(cur_map, order[i])
    i = (i+1) % len(order)
  return weight(cur_map)


if __name__ == "__main__":
  f = open("in_14.txt", "r")
  lines = f.readlines()
  lines = [line.strip() for line in lines]

  start = time.time()
  print(f"Solution to Task 1: {task_1(lines)}")
  print(f"Elapsed time: {(time.time()-start)*1000:.2f}ms")

  #print(task_2(lines))
  #print(task_2_cheating(lines, 9, 28))
  print(task_2_cheating(lines, 476, 288))
  f.close()