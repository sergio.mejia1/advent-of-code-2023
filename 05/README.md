Solution to Day 05 of Advent Of Code 2023

Moral of the story: 
> Why spend 10 seconds coding brute-force and waiting 10 minutes for it to finish when you can spend a lot more time thinking and coding so the solution finishes in <100ms?