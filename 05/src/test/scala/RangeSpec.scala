import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class RangeSpec extends AnyFlatSpec with should.Matchers {

  "Case 1. Intersecting two ranges" should "create two ranges" in {
    /*    3456789ABC
          X-------->
        X---->
        123456
     */
    val origin      = Range(1, 6)
    val destination = Range(3, 10)
    val offset      = 100
    (origin intersects (destination, offset)) should contain theSameElementsAs List(
      Range(1, 2),
      Range(100, 4, mapped = true)
    )
  }

  "Case 2. Intersecting two ranges" should "create three ranges" in {
    /*    3456789ABC
          X-------->
        X------------->
        123456789ABCDEF
     */
    val origin      = Range(1, 15)
    val destination = Range(3, 10)
    val offset      = 100
    (origin intersects (destination, offset)) should contain theSameElementsAs List(
      Range(1, 2),
      Range(100, 10, mapped = true),
      Range(13, 3)
    )
  }

  "Case 3. Intersecting two ranges" should "create two ranges" in {
    /*    3456789ABC
          X-------->
                 X---->
                 ABCDEF
     */
    val origin      = Range(10, 6)
    val destination = Range(3, 10)
    val offset      = 100
    (origin intersects (destination, offset)) should contain theSameElementsAs List(
      Range(13, 3),
      Range(107, 3, mapped = true)
    )
  }

  "Case 4. Intersecting two ranges" should "create one range" in {
    /*    3456789ABC
          X-------->
            X---->
            56789A
     */
    val origin      = Range(5, 6)
    val destination = Range(3, 10)
    val offset      = 100
    (origin intersects (destination, offset)) should contain theSameElementsAs List(
      Range(102, 6, mapped = true)
    )
  }

  "Case nil-1. Intersecting two non-overlapping ranges" should "return as is" in {
    val origin      = Range(1, 10)
    val destination = Range(100, 10)
    val offset      = 100
    (origin intersects (destination, offset)) should contain theSameElementsAs List(
      origin
    )
  }

  "Case nil-2. Intersecting two non-overlapping ranges" should "return as is" in {
    val origin      = Range(100, 10)
    val destination = Range(10, 10)
    val offset      = 100
    (origin intersects (destination, offset)) should contain theSameElementsAs List(
      origin
    )
  }
}
