
trait Solver {
  def solve(input: List[String]): Either[Throwable, String]
}