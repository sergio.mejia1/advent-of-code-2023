case class RangeResourceMapper(
    destinationRangeStart: Long,
    sourceRange: Range) {

  def map(resource: Range): List[Range] = resource.intersects(sourceRange, destinationRangeStart)
}

object RangeResourceMapper {
  def apply(input: String): RangeResourceMapper = {
    val values = input.trim.split(" ").map(_.toLong)
    new RangeResourceMapper(values.head, Range(values(1), values.last))
  }
}

case class RangeResourceMapperSet(originType: String, destinationType: String, mappers: List[RangeResourceMapper]) {
  def map(resource: Range): List[Range] = {
    // Start resource as non mapped to erase previous history
    mappers.foldLeft(List(resource.copy(mapped = false)))((mappedResource, mapper) => {
      // Only try to map ranges that haven't been already mapped
      val (alreadyMapped, nonMapped) = mappedResource.partition(_.mapped)
      nonMapped.flatMap(mapper.map) ++ alreadyMapped
    })
  }
}

object RangeResourceMapperSet {

  /* Lines in the form of
   *
   * <sourceResource>-to-<destinationResource> map:
   * 45 77 23
   * 81 45 19
   * dst src len
   * ...
   */

  /** Creates set of mapper of ranges
    * @param input Input string
    * @return RangeResourceMapperSet including the list of mappers (src+len) -> (dst+len)
    */
  def apply(input: List[String]): RangeResourceMapperSet = {
    val matches               = "^([\\w]+)-to-([\\w]+) map:$".r.findFirstMatchIn(input.head).get
    val (source, destination) = (matches.group(1), matches.group(2))
    new RangeResourceMapperSet(source, destination, input.tail.map(RangeResourceMapper(_)))
  }
}
