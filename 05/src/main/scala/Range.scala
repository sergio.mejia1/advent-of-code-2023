case class Range(start: Long, length: Long, mapped: Boolean = false) {
  def intersects(otherRange: Range, offset: Long): List[Range] = {
    if (start < otherRange.start && start + length >= otherRange.start) {
      // Case 1 or 2. I start before the other
      /*
            X--------->
         X------>

         OR

            X--------->
         X--------------->
       */
      if (start + length > otherRange.start + otherRange.length) {
        // Case 2. I start before and finish after: Three ranges
        val deltaX = otherRange.start - start
        val deltaR = otherRange.length
        val deltaE = length - deltaR - deltaX
        List(
          Range(start, deltaX),
          Range(start + deltaX + deltaR, deltaE),
          Range(offset, deltaR, mapped = true)
        )
      } else {
        // Case 1. I start before but end in the other range: Two ranges
        val deltaX = otherRange.start - start
        val deltaR = length - deltaX
        List(
          Range(start, deltaX),
          Range(offset, deltaR, mapped = true)
        )
      }
    } else if (start >= otherRange.start && start < otherRange.start + otherRange.length) {
      // Case 3 or 4. I start inside the other range
      /*
           X--------->
               X----------->

         OR

           X--------->
              X--->
       */
      if (start + length > otherRange.start + otherRange.length) {
        // Case 3. I end after the other range: Two ranges
        val deltaX = start - otherRange.start
        val deltaR = otherRange.start + otherRange.length - start
        val deltaE = length - deltaR
        List(
          Range(start + deltaR, deltaE),
          Range(offset + deltaX, deltaR, mapped = true)
        )
      } else {
        // Case 4. I am completely inside the other range
        val deltaX = start - otherRange.start
        List(Range(offset + deltaX, length, mapped = true))
      }
    } else {
      // Case nil. Ranges do not intersect
      List(copy(mapped = false))
    }
  }

}
