import scala.util.{Failure, Success, Try, Using}
import scala.io.Source

object Main {
  def main(args: Array[String]): Unit = {
    val tryLines: Try[List[String]] = Using(Source.fromFile("in_05.txt")) { source =>
      source.getLines.toList
    }
    tryLines match {
      case Success(lines) =>
        Task1Solver.solve(lines) match {
          case Left(error)  => error.printStackTrace()
          case Right(value) => println(s"Task 1 solution: $value")
        }
        val start = System.currentTimeMillis()
        Task2Solver.solve(lines) match {
          case Left(error)  => error.printStackTrace()
          case Right(value) => println(s"Task 2 solution: $value")
        }
        val end   = System.currentTimeMillis() - start
        println(s"Spent time: ${end}ms")
      case Failure(th)    => th.printStackTrace()
    }
  }
}
