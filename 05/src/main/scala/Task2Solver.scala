import scala.collection.mutable
import scala.util.Try

object Task2Solver extends Solver {
  override def solve(input: List[String]): Either[Throwable, String] = Try {
    val seeds = "\\d+".r.findAllMatchIn(input.head).toList.map(_.group(0).toLong)

    // Ugly imperative way to parse the input, how should I improve it?
    var l     = input
    val list  = mutable.ListBuffer.empty[RangeResourceMapperSet]
    while (l.nonEmpty) {
      val firstEmpty     = l.indexOf("")
      val maybeNextEmpty = l.indexOf("", firstEmpty + 1)
      val nextEmpty      = if (maybeNextEmpty == -1) l.size else maybeNextEmpty
      val lines          = l.slice(firstEmpty + 1, nextEmpty)
      list += RangeResourceMapperSet(lines)
      l = l.slice(nextEmpty, l.size)
    }

    val seedsRange = seeds.grouped(2).map(l => Range(l.head, l.last)).toList

    list.toList
      .foldLeft(seedsRange)((mapped, mapperSet) => mapped.flatMap(mapperSet.map))
      .min(Ordering.by[Range, Long](_.start))
      .start
      .toString

  }.toEither
}
