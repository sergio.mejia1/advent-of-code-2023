import scala.collection.mutable
import scala.util.Try

object Task1Solver extends Solver {
  override def solve(input: List[String]): Either[Throwable, String] = Try {
    val seeds = "\\d+".r.findAllMatchIn(input.head).toList.map(_.group(0).toLong)
    var l     = input
    val list  = mutable.ListBuffer.empty[ResourceMapperSet]
    while (l.nonEmpty) {
      val firstEmpty     = l.indexOf("")
      val maybeNextEmpty = l.indexOf("", firstEmpty + 1)
      val nextEmpty      = if (maybeNextEmpty == -1) l.size else maybeNextEmpty
      val lines          = l.slice(firstEmpty + 1, nextEmpty)
      list += ResourceMapperSet(lines)
      l = l.slice(nextEmpty, l.size)
    }

    list.toList
      .foldLeft(seeds) { (mapped, mapperSet) =>
        {
          val newMapped = mapped.map(mapperSet.map)
          newMapped
        }
      }
      .min
      .toString

  }.toEither
}
