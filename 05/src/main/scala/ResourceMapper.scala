case class Mapper(
    destinationRangeStart: Long,
    sourceRangeStart: Long,
    rangeLength: Long) {

  def map(resource: Long): Option[Long] = {
    resource match {
      case res
          if (res >= sourceRangeStart &&
            res < sourceRangeStart + rangeLength) =>
        Some(destinationRangeStart + (res - sourceRangeStart))
      case _ => None
    }
  }
}

object Mapper {
  def apply(input: String): Mapper = {
    val values = input.trim.split(" ").map(_.toLong)
    Mapper(values.head, values(1), values.last)
  }
}

case class ResourceMapperSet(originType: String, destinationType: String, mappers: List[Mapper]) {
  def map(resource: Long): Long = {
    mappers.foldLeft(resource)((mappedResource, mapper) => {
      mapper.map(mappedResource) match {
        case Some(value) => return value
        case None        => mappedResource
      }

    })
  }
}

object ResourceMapperSet {

  /* Lines in the form of
   *
   * <sourceResource>-to-<destinationResource> map:
   * 45 77 23
   * 81 45 19
   * dst src len
   * ...
   */

  /** Creates set of mapper of single numbers
    *
    * @param input
    *   Input string
    * @return
    *   ResourceMapperSet including the list of mappers (src+len) -> (dst+len)
    */
  def apply(input: List[String]): ResourceMapperSet = {
    val matches               = "^([\\w]+)-to-([\\w]+) map:$".r.findFirstMatchIn(input.head).get
    val (source, destination) = (matches.group(1), matches.group(2))
    new ResourceMapperSet(source, destination, input.tail.map(Mapper(_)))
  }
}
