import scala.util.Try

object Task1Solver extends Solver {
  override def solve(input: List[String]): Either[Throwable, String] = Try {
    input
      .map(l => {
        val (card, _, _) = ScratchCard(l)
        card
      })
      .foldRight(0)((card, sum) => card.score + sum)
      .toString
  }.toEither

}
