case class ScratchCard(code: Int, winningNumbers: Set[Int], chosenNumbers: Set[Int]) {
  val matchAmount: Int = winningNumbers.intersect(chosenNumbers).size

  val score: Int = {
    if (matchAmount == 0) 0 else scala.math.pow(2, matchAmount - 1).toInt
  }

}

object ScratchCard {
  def apply(line: String): (ScratchCard, Int, Int) = {
    val matches =
      "^Card +(\\d+): (.*) \\| (.*)$".r.findFirstMatchIn(line).get

    def extract(line: String): List[Int] = {
      "\\d+".r.findAllIn(line).toList.map(_.toInt)
    }

    val winning = extract(matches.group(2))
    val chosen  = extract(matches.group(3))
    (ScratchCard(matches.group(1).toInt, winning.toSet, chosen.toSet), winning.size, chosen.size)
  }
}
