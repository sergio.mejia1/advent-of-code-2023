import scala.util.Try

object Task2Solver extends Solver {
  override def solve(input: List[String]): Either[Throwable, String] = Try {
    val cards = input
      .map(l => {
        val (card, _, _) = ScratchCard(l)
        card
      })

    /*
      Fancy (a.k.a. functional) way of doing DP:
        list = cards.size amount of 1s
        for i <- [0,cards.size)
          add to positions [i+1, i+1+cards.matchAmount] of the list the amount present on list[i]
        sum the values on list
     */
    cards
      .foldLeft(List.fill(cards.size)(1)) { (list, card) =>
        (card.code until (card.code + card.matchAmount)).foldLeft(list) { (newList, i) =>
          newList.updated(i, newList(i) + newList(card.code - 1))
        }
      }
      .sum
      .toString
  }.toEither
}
