import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

import scala.io.Source
import scala.util.Using

class ScratchCardSpec extends AnyFlatSpec with should.Matchers {

  "chosen and winning lists" should "be sets" in {
    val lines: List[String] = Using(Source.fromFile("in_04.txt")) { source =>
      source.getLines.toList
    }.get

    lines.foreach { line =>
      val (card, winningSize, chosenSize) = ScratchCard(line)
      winningSize shouldEqual 10
      chosenSize shouldEqual 25
      card.winningNumbers.size shouldEqual winningSize
      card.chosenNumbers.size shouldEqual chosenSize
    }
  }
}
