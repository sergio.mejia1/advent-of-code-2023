ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.12"

ThisBuild / libraryDependencies := Seq(
  "org.scalatest"  %% "scalatest" % "3.2.11"
)

lazy val root = (project in file("."))
  .settings(
    name := "day_04"
  )
